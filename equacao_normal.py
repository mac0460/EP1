#NUSP 9298281

import numpy as np

def normal_equation_prediction(X, y):
    """
    Calculates the prediction using the normal equation method.
    You should add a new column with 1s.

    :param X: design matrix
    :type X: np.array
    :param y: regression targets
    :type y: np.array
    :return: prediction
    :rtype: np.array
    """
    n, d = X.shape
    Ones = np.ones((n, d))
    X1 = np.hstack((Ones, X))
    Xt = np.transpose(X1)
    XtXinv = np.linalg.inv(np.dot(Xt, X1))
    w = np.dot(np.dot(XtXinv, Xt), y)
    return np.dot(X1, w)
